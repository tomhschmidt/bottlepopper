//
//  BPAppDelegate.m
//  BottlePopper
//
//  Created by Thomas Schmidt on 4/22/14.
//  Copyright (c) 2014 BottlePopper. All rights reserved.
//

#import "BPAppDelegate.h"
#import <Parse/Parse.h>
#import "BPMedsViewController.h"

@implementation BPAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    self.rfidController = [[BPRFIDController alloc] init];
    [Parse setApplicationId:@"dLq6bdI0d5EtDaCuiVe5O64LezwlEXKTrvRg0dww"
                  clientKey:@"4X9a4Qy4euh57FqoVrgfjyMm3WS4KTsZCAIykpls"];
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    NSLog(@"app did enter background");
}

- (void)application:(UIApplication *)application
didReceiveLocalNotification:(UILocalNotification *)notification {
    
    NSLog(@"Received remote notification %@", notification);
    UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController ;
    BPMedsViewController *medsVC = (BPMedsViewController *)[navigationController.viewControllers lastObject];
    
    [medsVC reminderWatchAlert];
    [[UIApplication sharedApplication] cancelLocalNotification:notification];
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end

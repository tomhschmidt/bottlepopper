//
//  BPMedsView.m
//  BottlePopper
//
//  Created by Andy Mai on 5/15/14.
//  Copyright (c) 2014 BottlePopper. All rights reserved.
//

#import "BPMedsView.h"
#import "BPEvent.h"

@implementation BPMedsView

- (void)setup
{
    self.backgroundColor = nil;
    self.opaque = NO;
    self.contentMode = UIViewContentModeRedraw;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    CGPoint center = CGPointMake(CGRectGetMidX([self bounds]), CGRectGetMidY([self bounds]));
    SHPieChartView *normalPieChart = [[SHPieChartView alloc] initWithFrame:CGRectMake(10, 230, 300, 300)];
    
    [normalPieChart addAngleValue:0.25 andClolor:[self getPieChartColorFor:@"morning"]];
    [normalPieChart addAngleValue:0.25 andClolor:[self getPieChartColorFor:@"noon"]];
    [normalPieChart addAngleValue:0.25 andClolor:[self getPieChartColorFor:@"evening"]];
    [normalPieChart addAngleValue:0.25 andClolor:[self getPieChartColorFor:@"night"]];
    [normalPieChart setCenter:center];
    
    [self addSubview:normalPieChart];
    
    SHPieChartView *middleCircle = [[SHPieChartView alloc] initWithFrame:CGRectMake(10, 230, 50, 50)];
    [middleCircle addAngleValue:1 andClolor:[UIColor colorWithRed:189/255.0 green:195/255.0 blue:199/255.0 alpha:1]];
    [middleCircle setCenter:center];
    
    [self addSubview:middleCircle];
    
    [self drawLabels];
}

- (NSInteger)getCurrentHour
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate date]];
    NSInteger currentHour = [components hour];
//    NSInteger currentMinute = [components minute];
//    NSInteger currentSecond = [components second];
    return currentHour;
}

- (BOOL)useSelectedColorForTime:(NSString *)time currentHour:(NSInteger)currentHour
{
    if ([time isEqualToString:@"morning"] && (currentHour >= 6 && currentHour < 12)) {
        return YES;
    } else if ([time isEqualToString:@"noon"] && (currentHour >= 12 && currentHour < 18)) {
        return YES;
    } else if ([time isEqualToString:@"evening"] && (currentHour >= 18 && currentHour < 24)) {
        return YES;
    } else if ([time isEqualToString:@"night"] && (currentHour >= 0 && currentHour < 6)) {
        return YES;
    } else {
        return NO;
    }

}

- (UIColor *)getPieChartColorFor:(NSString *)time
{
    NSInteger currentHour = [self getCurrentHour];
    
    if ([self useSelectedColorForTime:time currentHour:currentHour]) {
        return [UIColor colorWithRed:41/255.0 green:128/255.0 blue:185/255.0 alpha:0.5];
    } else {
        return [UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:0.5];
    }
}

- (void)drawMeds:(NSDictionary *)meds
{
    if (meds[@"night"]) {
        CGRect frame3 = CGRectMake(70, 210, 100, 100);
        [self drawMeds:meds[@"night"] withFrame:frame3];
    }
    
    if (meds[@"morning"]) {
        CGRect frame1 = CGRectMake(190, 210, 100, 100);
        [self drawMeds:meds[@"morning"] withFrame:frame1];
    }

    if (meds[@"evening"]) {
        CGRect frame2 = CGRectMake(70, 330, 100, 100);
        [self drawMeds:meds[@"evening"] withFrame:frame2];
    }
    
    if (meds[@"noon"]) {
        CGRect frame = CGRectMake(190, 330, 100, 100);
        [self drawMeds:meds[@"noon"] withFrame:frame];
    }
}

- (void)drawMeds:(NSArray *)meds withFrame:(CGRect)frame
{
    UITextView *textView = [[UITextView alloc] initWithFrame:frame];
    NSString *medsString = [[NSString alloc] init];
    for (BPEvent *event in meds) {
        NSString* medString = [[event medication] name];
        medsString = [medsString stringByAppendingString:medString];
        medsString = [medsString stringByAppendingString:@"\n"];
    }
    
    textView.text = medsString;
    [textView setFont:[UIFont boldSystemFontOfSize:16.0f]];
    textView.backgroundColor = [UIColor clearColor];
    textView.textColor = [UIColor darkGrayColor];
    
    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    
    [self addSubview:textView];
}

#define LEFT 30
#define RIGHT 250
#define TOP 120
#define BOTTOM 420

- (void)drawLabels
{
    [self drawTopLeftLabel];
    [self drawBottomLeftLabel];
    [self drawTopRightLabel];
    [self drawBottomRightLabel];
}

- (void)drawTopLeftLabel
{
    CGRect labelFrame = CGRectMake( LEFT, TOP, 100, 30 );
    UILabel* label = [[UILabel alloc] initWithFrame: labelFrame];
    [label setText: @"Night"];
    [label setTextColor: [self getLabelColorFor:@"night"]];
    [self addSubview: label];
}

- (void)drawBottomLeftLabel
{
    CGRect labelFrame = CGRectMake( LEFT, BOTTOM, 100, 30 );
    UILabel* label = [[UILabel alloc] initWithFrame: labelFrame];
    [label setText: @"Evening"];
    [label setTextColor: [self getLabelColorFor:@"evening"]];
    [self addSubview: label];
}

- (void)drawTopRightLabel
{
    CGRect labelFrame = CGRectMake( RIGHT, TOP, 100, 30 );
    UILabel* label = [[UILabel alloc] initWithFrame: labelFrame];
    [label setText: @"Morning"];
    [label setTextColor: [self getLabelColorFor:@"morning"]];
    [self addSubview: label];
}

- (void)drawBottomRightLabel
{
    CGRect labelFrame = CGRectMake( RIGHT, BOTTOM, 100, 30 );
    UILabel* label = [[UILabel alloc] initWithFrame: labelFrame];
    [label setText: @"Noon"];
    [label setTextColor: [self getLabelColorFor:@"noon"]];
    [self addSubview: label];
}

- (UIColor *)getLabelColorFor:(NSString *)time
{
    NSInteger currentHour = [self getCurrentHour];
    
    if ([self useSelectedColorForTime:time currentHour:currentHour]) {
        return [UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1];
    } else {
        return [UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:0.6];
    }
}

@end

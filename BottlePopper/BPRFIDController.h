//
//  BPRFIDController.h
//  BottlePopper
//
//  Created by Thomas Schmidt on 5/9/14.
//  Copyright (c) 2014 BottlePopper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface BPRFIDController : NSObject<CBCentralManagerDelegate, CBPeripheralDelegate>

@end

@interface BPRFIDControllerDelegate : NSObject

@end

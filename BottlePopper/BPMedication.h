//
//  BPMedication.h
//  BottlePopper
//
//  Created by Thomas Schmidt on 5/9/14.
//  Copyright (c) 2014 BottlePopper. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BPMedication : NSObject

@property NSString* name;
@property NSString* type;
@property NSString* rfidTagUUID;

@end

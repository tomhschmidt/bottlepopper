//
//  main.m
//  BottlePopper
//
//  Created by Thomas Schmidt on 4/22/14.
//  Copyright (c) 2014 BottlePopper. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BPAppDelegate class]));
    }
}

//
//  BPAppDelegate.h
//  BottlePopper
//
//  Created by Thomas Schmidt on 4/22/14.
//  Copyright (c) 2014 BottlePopper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BPRFIDController.h"

@interface BPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) BPRFIDController* rfidController;

@end

//
//  BPUser.m
//  BottlePopper
//
//  Created by Andy Mai on 5/16/14.
//  Copyright (c) 2014 BottlePopper. All rights reserved.
//

#import "BPUser.h"

@implementation BPUser

- (NSString *)fullName
{
    NSString *fullName = [self.firstName stringByAppendingString:@" "];
    fullName = [fullName stringByAppendingString:self.lastName];
    
    return fullName;
}

+ (BPUser *)getBPUserWithId:(NSString *)userId
{
    
    return nil;
}


@end

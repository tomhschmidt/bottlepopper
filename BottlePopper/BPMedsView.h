//
//  BPMedsView.h
//  BottlePopper
//
//  Created by Andy Mai on 5/15/14.
//  Copyright (c) 2014 BottlePopper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SHPieChartView.h"

@interface BPMedsView : UIView
- (void)drawMeds:(NSDictionary *)meds;
@end

//
//  BPMedsViewController.m
//  BottlePopper
//
//  Created by Andy Mai on 5/15/14.
//  Copyright (c) 2014 BottlePopper. All rights reserved.
//

#import "BPMedsViewController.h"
#import "BPSchedule.h"
#import "BPEvent.h"
#import <Parse/Parse.h>

@interface BPMedsViewController ()
@property (weak, nonatomic) IBOutlet BPMedsView *medView;
@property NSString* username;
@property NSString* lastName;
@property (strong, nonatomic) NSMutableDictionary *meds;
@property (strong, nonatomic) BPSchedule *schedule;
@property (strong, nonatomic) PBWatch* watch;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *currDateLabel;

@end

@implementation BPMedsViewController

- (NSMutableDictionary *)meds
{
    if (!_meds) {
        _meds = [[NSMutableDictionary alloc] init];
    }
    
    return _meds;
}

- (PBWatch *)watch
{
    if (!_watch) {
        _watch = [[PBWatch alloc] init];
    }
    
    return _watch;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.nameLabel.text = self.firstName;
    self.nameLabel.textColor = [UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1];
    self.currDateLabel.text = [self getCurrentDate];
    // Do any additional setup after loading the view.
    NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];
    _schedule = [[BPSchedule alloc] initWithUserId:userId handler:^(NSError * error) {
        NSLog(@"All loaded");
        [self medsViewHandler];
    }];
}

- (void)didReadBottle:(NSNotification*)bottleNotif {
    NSLog(@"Read bottle %@", bottleNotif.object);
    
    NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];
    
    PFQuery *query = [PFQuery queryWithClassName:@"Medication"];
    [query whereKey:@"userId" equalTo:[PFObject objectWithoutDataWithClassName:@"_User" objectId:userId]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSLog(@"%@",objects);
        
        PFObject *bottle = [self containsBottle:bottleNotif.object inBottles:objects];
        if (bottle != nil) {
            // Send confirmation message to Pebble
            [self confirmMedAlert:bottle[@"name"]];
        } else {
            // Alert and vibrate Pebble
            [self wrongMedAlert:bottle[@"name"]];
        }
        
        if (error) {
            NSLog(@"%@",error);
        }
    }];
}

- (PFObject *)containsBottle:(NSString *)bottleId inBottles:(NSArray *)bottles
{
    for (PFObject *bottle in bottles) {
        if ([bottleId isEqualToString:[bottle valueForKey:@"rfidTagUUID"]]) {
            return bottle;
        }
    }
    
    return nil;
}

- (void)medsViewHandler
{
    [self getMeds];
    [self.medView drawMeds:self.meds];
    
    self.watch = [[PBPebbleCentral defaultCentral] lastConnectedWatch];
    NSLog(@"Last connected watch: %@", self.watch);
    
    uuid_t appUUIDBytes;
    NSUUID* appUUID = [[NSUUID alloc] initWithUUIDString:@"7e4e1a2d-3676-47a8-9fca-7d64518ff5b4"];
    [appUUID getUUIDBytes:appUUIDBytes];
    [[PBPebbleCentral defaultCentral] setAppUUID:[NSData dataWithBytes:appUUIDBytes length:16]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReadBottle:) name:@"didReadBottle" object:nil];
    
    /* Local Notification for Demo Purposes */
//    UILocalNotification *notif = [[UILocalNotification alloc] init];
//    
//    NSDate *alertTime = [[NSDate date]
//                         dateByAddingTimeInterval:5];
//    notif.fireDate = alertTime;
//    notif.timeZone = [NSTimeZone defaultTimeZone];
//    
//    notif.alertBody = @"Did you forget something?";
//    notif.alertAction = @"Show me";
//    [[UIApplication sharedApplication] scheduleLocalNotification:notif];
}

- (void)confirmMedAlert:(NSString *)med
{
    NSString *message = @"4Correct Bottle:\n";
    message = [message stringByAppendingString:med];
    
    [self notifyWatch:message];
}

- (void)wrongMedAlert:(NSString *)wrongMed
{
    NSString *message = [NSString stringWithFormat:@"3WRONG Bottle!\nDrop the bottle now!"];
    [self notifyWatch:message];
}

- (void)missedMedWatchAlert:(NSString *)missedMed
{
    NSString *message = [NSString stringWithFormat:@"2MISSED\nYou forgot to take %@ today!", missedMed];
    [self notifyWatch:message];
}

- (void)reminderWatchAlert
{
    NSString *message = @"1REMINDER\n";
    NSString *currentMeds = [self getCurrentMedsString];
    message = [message stringByAppendingString:currentMeds];
    
    [self notifyWatch:message];
}

- (void)notifyWatch:(NSString *)message
{
    [self.watch appMessagesGetIsSupported:^(PBWatch *watch, BOOL isAppMessagesSupported) {
        if (isAppMessagesSupported) {
            [self.watch appMessagesLaunch:^(PBWatch *watch, NSError *error) {
                if (!error) {
                    NSLog(@"Successfully launched app.");
                    [self sendMedicationMessage:message];
                }
                else {
                    NSLog(@"Error launching app - Error: %@", error);
                }
            }
             ];
        } else {
            NSLog(@"This Pebble does not support app message");
        }
    }];

}

- (void)sendMedicationMessage:(NSString *)message
{
    
    NSDictionary *update = @{ @(0):[NSNumber numberWithUint8:42],
                              @(1):message };
    [self.watch appMessagesPushUpdate:update onSent:^(PBWatch *watch, NSDictionary *update, NSError *error) {
        if (!error) {
            NSLog(@"Successfully sent message.");
        }
        else {
            NSLog(@"Error sending message: %@", error);
        }
    }];
}

- (NSInteger)getCurrentHour
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate date]];
    NSInteger currentHour = [components hour];
    return currentHour;
}

- (TimeFrame)getTimeFrameFor:(NSInteger)hour
{
    if (hour >= 6 && hour < 12) {
        return kMorning;
    } else if (hour >= 12 && hour < 18) {
        return kAfternoon;
    } else if (hour >= 18 && hour < 24) {
        return kEvening;
    } else {
        return kNight;
    }
}

- (NSString *)getKeyFromTimeFrame:(TimeFrame)frame
{
    if (frame == kMorning) {
        return @"morning";
    } else if (frame == kAfternoon) {
        return @"noon";
    } else if (frame == kEvening) {
        return @"evening";
    } else {
        return @"night";
    }
}

- (NSString *)getCurrentMedsString
{
    NSInteger currentHour = [self getCurrentHour];
    TimeFrame currTimeFrame = [self getTimeFrameFor:currentHour];
    NSString *result = @"";
    
    for (BPEvent *event in self.meds[[self getKeyFromTimeFrame:currTimeFrame]]) {
        result = [result stringByAppendingString:[[event medication] name]];
        result = [result stringByAppendingString:@"\n"];
    }
    
    return result;
}

- (int)getCurrentDayOfWeek
{
    NSDate *today = [NSDate date];
    NSDateFormatter *myFormatter = [[NSDateFormatter alloc] init];
    [myFormatter setDateFormat:@"c"]; // day number, like 7 for saturday
    
    int dayOfWeek = [[myFormatter stringFromDate:today] intValue];
    
    return dayOfWeek;
}

- (NSString *)getCurrentDate
{
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"EEE, MMM d"];
    return [dateFormat stringFromDate:date];
}

- (void)getMeds
{
    int dayOfWeekInt = [self getCurrentDayOfWeek];
    DayOfWeek day = dayOfWeekInt;
    
    TimeFrame time = kMorning;
    NSArray *morningMeds = [_schedule medicationsForDay:day timeFrame:time];
    if (morningMeds) {
        self.meds[@"morning"] = morningMeds;
    }
    
    time = kAfternoon;
    NSArray *afternoonMeds = [_schedule medicationsForDay:day timeFrame:time];
    if (afternoonMeds) {
        self.meds[@"noon"] = afternoonMeds;
    }
    
    time = kEvening;
    NSArray *eveningMeds = [_schedule medicationsForDay:day timeFrame:time];
    if (eveningMeds) {
        self.meds[@"evening"] = eveningMeds;
    }
    
    time = kNight;
    NSArray *nightMeds = [_schedule medicationsForDay:day timeFrame:time];
    if (nightMeds) {
        self.meds[@"night"] = nightMeds;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

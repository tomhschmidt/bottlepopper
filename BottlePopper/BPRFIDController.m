//
//  BPRFIDController.m
//  BottlePopper
//
//  Created by Thomas Schmidt on 5/9/14.
//  Copyright (c) 2014 BottlePopper. All rights reserved.
//

#import "BPRFIDController.h"

@interface BPRFIDController()

@property CBPeripheral* reader;
@property CBCentralManager* centralManager;

@property NSString* lastReadBottleId;
@property NSTimer* bottleResetTimer;
@property int numTimesRead;

@end

@implementation BPRFIDController

NSString* biscuitId = @"DBA35E1D-918E-5A49-E243-BA180C696E6A";
NSString* rfidCharacteristicId = @"713D0002-503E-4C75-BA94-3148F18D941E";
NSString* rfidServiceId = @"713D0000-503E-4C75-BA94-3148F18D941E";

int NUM_READ_THRESHOLD = 6;
int TIME_THRESHOLD = 7;

- (BPRFIDController*)init{
    self = [super init];
    if (self) {
        _centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
        _lastReadBottleId = @"";

    }
    return self;
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    NSLog(@"Did update state %d", central.state);
    if(central.state == CBCentralManagerStatePoweredOn) {
        NSLog(@"Scanning");
        [_centralManager scanForPeripheralsWithServices:nil options:nil];
    }
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    //NSLog(@"%@ %@ %@", peripheral.name, peripheral.identifier, advertisementData);
    if([[peripheral.identifier UUIDString] isEqualToString:biscuitId]) {
        NSLog(@"Found Biscuit");
        _reader = peripheral;
        [_centralManager connectPeripheral:_reader options:nil];
    }
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    NSLog(@"Connected to Biscuit");
    [_reader setDelegate:self];
    [_reader discoverServices:nil];
}

-(void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    if([[peripheral.identifier UUIDString] isEqualToString:biscuitId]) {
        NSLog(@"Biscuit disconnected");
        [central scanForPeripheralsWithServices:nil options:nil];
    }
    
}

-(void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    if([[peripheral.identifier UUIDString] isEqualToString:biscuitId]) {
        NSLog(@"Biscuit failed to connect");
        [central scanForPeripheralsWithServices:nil options:nil];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    if(characteristic.value != nil) {
        NSString* curBottleId = [characteristic.value description];
        if([_lastReadBottleId isEqualToString:curBottleId]) {
            _numTimesRead++;
            if(_numTimesRead == NUM_READ_THRESHOLD) {
                NSLog(@"Read bottle %@", curBottleId);
                [[NSNotificationCenter defaultCenter] postNotificationName:@"didReadBottle" object:curBottleId];
                _bottleResetTimer = [NSTimer scheduledTimerWithTimeInterval:TIME_THRESHOLD target:self selector:@selector(resetCurBottle) userInfo:nil repeats:NO];
            }
        } else {
            if(_bottleResetTimer) {
                [_bottleResetTimer invalidate];
            }
            _lastReadBottleId = curBottleId;
            _numTimesRead = 1;
        }
        NSLog(@"%@", characteristic.value);
    }
}

-(void)resetCurBottle{
    _numTimesRead = 0;
    _lastReadBottleId = @"";
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    NSLog(@"Found services for Biscuit");
    for(int i = 0; i < [peripheral.services count]; i++) {
        [peripheral discoverCharacteristics:nil forService:[peripheral.services objectAtIndex:i]];
    }
    if(error) {
        NSLog(@"%@", error);
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    NSLog(@"Found characteristics for service for Biscuit");
    for(int i = 0; i < [service.characteristics count]; i++) {
        CBCharacteristic* characteristic = [service.characteristics objectAtIndex:i];
        if([[[characteristic UUID] UUIDString] isEqualToString:rfidCharacteristicId] ){
            [peripheral setNotifyValue:YES forCharacteristic:characteristic];
            NSLog(@"Subscribed to RFID updates");
        }
    }
}



@end

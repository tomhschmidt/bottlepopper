//
//  BPSchedule.h
//  BottlePopper
//
//  Created by Thomas Schmidt on 5/16/14.
//  Copyright (c) 2014 BottlePopper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BPEvent.h"

@protocol BPScheduleDelegate

-(void)eventFired:(BPEvent*)event;

@end

@interface BPSchedule : NSObject

@property NSObject<BPScheduleDelegate>* delegate;

-(id)initWithUserId:(NSString *)userId handler:(void (^)(NSError*))scheduleDidLoadHandler;
-(NSArray*)medicationsForDay:(DayOfWeek)day timeFrame:(TimeFrame)time;

@end


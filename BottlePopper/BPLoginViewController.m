//
//  BPLoginViewController.m
//  BottlePopper
//
//  Created by Thomas Schmidt on 5/9/14.
//  Copyright (c) 2014 BottlePopper. All rights reserved.
//

#import "BPLoginViewController.h"
#import <Parse/Parse.h>
#import "BPMedsViewController.h"
#import "FBShimmeringView.h"
#import "UIImage+StackBlur.h"

#define DEBUG YES

@interface BPLoginViewController ()
@property (weak, nonatomic) IBOutlet UILabel *grabBottleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *welcomeLabel;

@property FBShimmeringView* shimmeringView;

@end

@implementation BPLoginViewController



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIImage* unblurred = [UIImage imageNamed:@"background1.jpg"];
    UIImage* blurred = [unblurred stackBlur:15];
    self.backgroundImageView.image = blurred;
    _shimmeringView = [[FBShimmeringView alloc] initWithFrame:_grabBottleLabel.frame];
    [self.view addSubview:_shimmeringView];
    
    _shimmeringView.contentView = _grabBottleLabel;
    
    // Start shimmering.
    _shimmeringView.shimmering = YES;

    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] && !DEBUG) {
        // We already have a logged in user
        [self performSegueWithIdentifier:@"segueToMedsView" sender:self];
    } else {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReadBottle:) name:@"didReadBottle" object:nil];
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didReadBottle:(NSNotification*)bottle {
    NSLog(@"Read bottle %@", bottle.object);
    PFQuery* medQuery = [PFQuery queryWithClassName:@"Medication"];
    [medQuery whereKey:@"rfidTagUUID" equalTo:bottle.object];
    PFQuery* userQuery = [PFQuery queryWithClassName:@"User"];
    [userQuery whereKey:@"objectId" matchesKey:@"userId" inQuery:medQuery];
    [_activityIndicator startAnimating];
    [self fadeOutLabel:_grabBottleLabel];
    [medQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        PFUser* user = [[objects objectAtIndex:0] objectForKey:@"userId"];
        [user fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            [_activityIndicator stopAnimating];
            [[NSUserDefaults standardUserDefaults] setObject:[object objectForKey:@"username"]forKey:@"username"];
            [[NSUserDefaults standardUserDefaults] setObject:[object objectForKey:@"firstName"]forKey:@"firstName"];
            [[NSUserDefaults standardUserDefaults] setObject:[object objectForKey:@"lastName"]forKey:@"lastName"];
            [[NSUserDefaults standardUserDefaults] setObject:[object objectId] forKey:@"userId"];
            [[NSNotificationCenter defaultCenter] removeObserver:self];
            _welcomeLabel.text = [NSString stringWithFormat:@"Welcome, %@",[object objectForKey:@"firstName"]];
            [self fadeInLabel:_welcomeLabel];

        }];

        
    }];
    
}

- (void)fadeOutLabel:(UILabel*)label {
    [UIView animateWithDuration:1.f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [label setAlpha:0.f];
    } completion:nil];
}

- (void)fadeInLabel:(UILabel*)label {
    if(label == _welcomeLabel) {
        [UIView animateWithDuration:2.f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [label setAlpha:1.f];
        } completion:^(BOOL success){
            NSTimer* timer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(segueToMedsView) userInfo:nil repeats:NO];
        }];
    } else {
        [UIView animateWithDuration:1.f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [label setAlpha:1.f];
        } completion:nil];
    }
}

- (void)segueToMedsView {
    [self performSegueWithIdentifier:@"segueToMedsView" sender:self];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"segueToMedsView"]) {
        if ([segue.destinationViewController isKindOfClass:[BPMedsViewController class]]) {
            BPMedsViewController *medsVC = (BPMedsViewController *)segue.destinationViewController;
            medsVC.firstName = [[NSUserDefaults standardUserDefaults] objectForKey:@"firstName"];
        }
    }
}


@end

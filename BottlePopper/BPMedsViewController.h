//
//  BPMedsViewController.h
//  BottlePopper
//
//  Created by Andy Mai on 5/15/14.
//  Copyright (c) 2014 BottlePopper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BPMedsView.h"
#import <PebbleKit/PebbleKit.h>

@interface BPMedsViewController : UIViewController
@property NSString *userId;
@property (strong, nonatomic) NSString *firstName;

- (void)reminderWatchAlert;
- (void)missedMedWatchAlert;
@end

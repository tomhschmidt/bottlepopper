//
//  BPSchedule.m
//  BottlePopper
//
//  Created by Thomas Schmidt on 5/16/14.
//  Copyright (c) 2014 BottlePopper. All rights reserved.
//

#import "BPSchedule.h"
#import <Parse/Parse.h>

@interface BPSchedule()

@property NSMutableDictionary* days;
@property NSMutableDictionary* medications;

@end

@implementation BPSchedule

-(id)initWithUserId:(NSString *)userId handler:(void (^)(NSError*))scheduleDidLoadHandler{
    
    self = [super init];
    if(self) {
        _days = [NSMutableDictionary dictionary];
        _medications = [NSMutableDictionary dictionary];
        
        for(DayOfWeek day = kSunday; day <= kSaturday; day++) {
            NSMutableDictionary* timeFrames = [NSMutableDictionary dictionary];
            for(TimeFrame time = kMorning; time <= kNight; time++) {
                [timeFrames setObject:[NSMutableArray array] forKey:[NSNumber numberWithInt:time]];
            }
            [_days setObject:timeFrames forKey:[NSNumber numberWithInt:day]];
        }
        
        
        PFQuery* query = [PFQuery queryWithClassName:@"Event"];
        [query includeKey:@"medication"];
        [query whereKey:@"user" equalTo:[PFQuery getUserObjectWithId:userId]];
        [query findObjectsInBackgroundWithBlock:^(NSArray *events, NSError *error) {
            
            NSDate* curDate = [NSDate date];
            for(PFObject* event in events) {
                PFObject* medication = [event objectForKey:@"medication"];
                if(![_medications objectForKey:medication.objectId]) {
                    BPMedication* newMed = [[BPMedication alloc] init];
                    newMed.name = [medication objectForKey:@"name"];
                    newMed.rfidTagUUID = [medication objectForKey:@"rfidTagUUID"];
                    newMed.type = [medication objectForKey:@"type"];
                    [_medications setObject:newMed forKey:medication.objectId];
                }
                
                NSArray* weekdays = [event objectForKey:@"weekdays"];
                for(int i = 0; i < [weekdays count]; i++) {
                    DayOfWeek dayOfWeek = [[weekdays objectAtIndex:i] intValue];
                    BPEvent* newEvent = [[BPEvent alloc] init];
                    newEvent.time = [[event objectForKey:@"time"] intValue];
                    newEvent.dayOfWeek = dayOfWeek;
                    newEvent.medication = [_medications objectForKey:medication.objectId];
                    TimeFrame timeFrame;
                    if(newEvent.time < 1200) {
                        timeFrame = kMorning;
                    } else if(newEvent.time < 1500) {
                        timeFrame = kAfternoon;
                    } else if(newEvent.time < 1800) {
                        timeFrame = kEvening;
                    } else {
                        timeFrame = kNight;
                    }
                    
                    [[[_days objectForKey:[NSNumber numberWithInt:dayOfWeek]] objectForKey:[NSNumber numberWithInt:timeFrame]] addObject:newEvent];
                    
                    UILocalNotification *notif = [[UILocalNotification alloc] init];
                    
                    notif.fireDate = [self getNextOccurenceOfEvent:newEvent fromDate:curDate];
                    notif.timeZone = [NSTimeZone defaultTimeZone];
                    
                    notif.alertBody = @"Did you forget something?";
                    notif.alertAction = @"Show me";
                    
                    [[UIApplication sharedApplication] scheduleLocalNotification:notif];
                }
            }
            
            scheduleDidLoadHandler(error);
        }];
    }
    
    return self;
}

-(NSDate*)getNextOccurenceOfEvent:(BPEvent*)event fromDate:(NSDate*)date {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents* comps = [calendar components:(NSYearCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSWeekdayCalendarUnit ) fromDate:date];
    
    int weekday = [comps weekday];
    int hour = [comps hour];
    int minutes = [comps minute];
    int totalTime = (hour * 100) + minutes;
    
    DayOfWeek day = event.dayOfWeek;
    int time = event.time;
    
    if(day > weekday) {
        [comps setDay:[comps day] + (day - weekday)];
        [comps setHour:(time / 100)];
        [comps setMinute:(time % 100)];
    } else if(day < weekday) {
        [comps setDay:[comps day] + (day - weekday) + 7];
        [comps setHour:(time / 100)];
        [comps setMinute:(time % 100)];
    } else {
        if(time > totalTime) {
            [comps setHour:(time / 100)];
            [comps setMinute:(time % 100)];
        } else {
            // It's already happened for this week
            [comps setDay:[comps day] + 7];
            [comps setHour:(time / 100)];
            [comps setMinute:(time % 100)];
        }
    }
    
    NSDate* parsedDate = [calendar dateFromComponents:comps];
    return parsedDate;
}

- (NSArray*)medicationsForDay:(DayOfWeek)day timeFrame:(TimeFrame)time {
    NSNumber* dayNumber = [NSNumber numberWithInteger:day];
    NSNumber* timeFrameNumber = [NSNumber numberWithInteger:time];
    
    return [[_days objectForKey:dayNumber] objectForKey:timeFrameNumber];
}

@end

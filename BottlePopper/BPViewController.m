//
//  BPViewController.m
//  BottlePopper
//
//  Created by Thomas Schmidt on 4/22/14.
//  Copyright (c) 2014 BottlePopper. All rights reserved.
//

#import "BPViewController.h"
#import <PebbleKit/PebbleKit.h>

@interface BPViewController ()

@property PBWatch* watch;

@property IBOutlet UIButton* vibrationButton;

@end

@implementation BPViewController

@synthesize watch;

uuid_t appUUIDBytes;

- (IBAction)vibrationButtonPressed:(id)sender {
    NSDictionary *update = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"ok", [NSNumber numberWithInt:0],
                            @"vibe", [NSNumber numberWithInt:1], nil];
    [self.watch appMessagesPushUpdate:update onSent:^(PBWatch *watch, NSDictionary *update, NSError *error) {
        if (!error) {
            NSLog(@"Successfully sent message.");
        }
        else {
            NSLog(@"Error sending message: %@", error);
        }
    }];
}
- (IBAction)openAppButtonPressed:(id)sender {
    [self.watch appMessagesLaunch:^(PBWatch *watch, NSError *error) {
        if (!error) {
            NSLog(@"Successfully launched app.");
        }
        else {
            NSLog(@"Error launching app - Error: %@", error);
        }
    }
     ];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    NSUUID* appUUID = [[NSUUID alloc] initWithUUIDString:@"7e4e1a2d-3676-47a8-9fca-7d64518ff5b4"];
    [appUUID getUUIDBytes:appUUIDBytes];
    
    [[PBPebbleCentral defaultCentral] setAppUUID:[NSData dataWithBytes:appUUIDBytes length:16]];
	// Do any additional setup after loading the view, typically from a nib.
    
    watch = [[PBPebbleCentral defaultCentral] lastConnectedWatch];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
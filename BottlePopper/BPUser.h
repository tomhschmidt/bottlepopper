//
//  BPUser.h
//  BottlePopper
//
//  Created by Andy Mai on 5/16/14.
//  Copyright (c) 2014 BottlePopper. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BPUser : NSObject
@property NSString* firstName;
@property NSString* lastName;
@property NSString* userId;

- (NSString *)fullName;
@end

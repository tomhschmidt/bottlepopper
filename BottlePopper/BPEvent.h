//
//  BPEvent.h
//  BottlePopper
//
//  Created by Thomas Schmidt on 5/9/14.
//  Copyright (c) 2014 BottlePopper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BPMedication.h"

typedef enum TimeFrame : NSUInteger {
    kMorning,
    kAfternoon,
    kEvening,
    kNight
} TimeFrame;

typedef enum DayOfWeek : NSUInteger {
    kSunday = 1,
    kMonday,
    kTuesday,
    kWednesday,
    kThursday,
    kFriday,
    kSaturday
} DayOfWeek;

@interface BPEvent : NSObject

@property BPMedication* medication;
@property DayOfWeek dayOfWeek;
@property TimeFrame timeFrame;
@property int time;
@property NSTimer* timer;


@end


